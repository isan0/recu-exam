var mongoose = require("mongoose"),
    Schema = mongoose.Schema;

var carSchema = new Schema({
    id: {type:String},
    brand: {type:String},
    model: {type:String},
    price: {type:String}
});

module.exports = mongoose.model("Car", carSchema);