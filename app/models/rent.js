var mongoose = require("mongoose"),
    Schema = mongoose.Schema;

var rentsSchema = new Schema({
    client: {type:String},
    car: {type:String},
    price: {type:String}
});

module.exports = mongoose.model("Rents", rentsSchema);