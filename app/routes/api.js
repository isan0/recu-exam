var path = require("path"),
  express = require("express"),
  router = express.Router(),
  ctrlDir = "/app/app/controllers",
  carsCtrl = require(path.join(ctrlDir, "cars")),
  rentsCtrl = require(path.join(ctrlDir, "rents")),
  clientsCtrl = require(path.join(ctrlDir, "clients"));

/* CARS API */

/* CLIENTS API */

/* RENTS API */
router.route('/rent/new').post((req,res,next)=>{
  var newRent = {
    client: req.body.client,
    car: req.body.car,
    price: req.body.price
  }
  rentsCtrl.save(newRent);
  res.redirect("/rent/list")
});

router.route('/rent/delete/:id').get(async(req,res,next)=>{
  rentsCtrl.delete(req.params.id);
  res.redirect("/rent/list");
});

module.exports = router;
