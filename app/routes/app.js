var express = require("express"),
  path = require("path"),
  router = express.Router(),
  ctrlDir = "/app/app/controllers",
  carsCtrl = require(path.join(ctrlDir, "cars")),
  rentsCtrl = require(path.join(ctrlDir, "rents")),
  clientsCtrl = require(path.join(ctrlDir, "clients")),
  chatCtrl = require(path.join(ctrlDir, "chat"));


router.route("/chat/:id").get(async(req,res,next)=>{
  var result = await chatCtrl.getAll();
  res.render("chat", result);
});

router.route("/rent/new").get(async(req,res,next)=>{
  var result = await carsCtrl.getAll();
  res.render("form", {all: result});
});

router.route("/rent/list").get(async(req,res,next)=>{
  var rent = await rentsCtrl.find();
  res.render("allRent",{allRent: rent});
});


module.exports = router;
