var mongoose = require("mongoose"),
  Model = require("../models/client");

exports.getAll = async (req, res, next) => {
  var res = await Model.getAll({})
  return res;
};

exports.find = async (req, res, next) => {
  var res = await Model.find({})
  return res;
};

exports.delete = async (req, res, next) => {
  var res = await Model.deleteOne()
  return res;
};
