var mongoose = require("mongoose"),
  Model = require("../models/car");

exports.getAll = async (req, res, next) => {
  var res = await Model.getAll({})
  return res;
};

exports.find = async (req, res, next) => {
  var res = await Model.find({})
  return res;
};

exports.create = async (req, res, next) => {
 
};
exports.delete = async (req, res, next) => {
  var res = await Model.deleteOne()
  return res;
};
